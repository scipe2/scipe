package scipe.common.ClientInterfaces;

import java.util.ArrayList;

public interface MessageDisplay {
	
	// Displays a list of users in the chat room
	public void showUsers(ArrayList<String> users);
	
	// Displays a list of all the chatrooms
	public void showChatrooms(ArrayList<String> chatrooms);
	
	public void showChat();
	
	// Displays the message
	public void showMessage(String msg);
	public void showChatroomMessage(String msg, String chatroom);
	
	// Displays the message from user;
	public void addMessage(String username, String msg);
	
	// Displays the message from user;
	// If on console, color must always be "black"
	public void addMessage(String username, String msg, String color);
	
	// Exits the display
	public void exit();
	
	// Displays the login screen once the user logs out or starts the client
	public void showLogin();
	
	// Displays the logout screen once the user logs out
	public void showLogout();
	
	/**
	 * The FeedbackListener is the class that deals with
	 * all the inputs that is sent through the Display.
	 */
	public void setFeedbackListener(FeedbackListener listener);
	
	public void showSignUp();

	public void showHome();

	public void specificMessage(String friend);

	
}