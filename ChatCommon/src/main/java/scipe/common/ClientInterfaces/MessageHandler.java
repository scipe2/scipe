package scipe.common.ClientInterfaces;

import java.util.HashMap;

import javax.jms.Destination;
import javax.jms.MessageListener;
import javax.jms.Session;

import scipe.common.Command.Command;

public interface MessageHandler {
	
	// Sets the message to the current destination
	// Returns True if the message was successfully sent
	public boolean sendMessage(String msg);
	
	// Sends the hash map as key value pairs
	public boolean sendMessages(HashMap<String, String> msgs);
	
	// Sets the destination that this MessageHandler will
	// communicate with
	public void setServer(String ip, int port);
	public void setServer(String ip);
	public void setServer();
	
	// Gets the current server receiving all the messages
	public Destination getServer();
	
	// Get the Destination(address) of this message handler
	public Destination getLocalDestination();

	// Returns True if the current server can be communicated with
	public boolean isConnectable();
	
	public boolean reconnect();
	
	// Sets the class in charge of handling all of the messages
	// received from the server
	public boolean setMessageListener(MessageListener listener);

	/* This send the a command to the server */
	public boolean sendCommand( Command cmd );

	public void sendChatroomMessage(String msg, String user);

}
