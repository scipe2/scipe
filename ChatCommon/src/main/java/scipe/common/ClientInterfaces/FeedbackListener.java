package scipe.common.ClientInterfaces;

import java.util.ArrayList;

public interface FeedbackListener {
	
	// The user clicked login with the following email and
	// password
	public void loginPerformed(String email, String password, String string);
	
	// The user clicked logout
	public void logoutPerformed();
	
	// The user added a message
	public void messageAdded(String msg);
	
	// The user joined the following chatroom
	public void chatroomJoined(String chatroom);
	
	/*these are all the things i added*/
	//the user signs up with the email and password
	public void signedUp(String email, String password);
	
	// probably have to take this out
	public void registerClicked();

	//user clicked resgister and added the following username and password
	public void registerPerformed(String username, String password, String string); 
		
	public void messageFriend(ArrayList<String> friends);

	public void refreshOnlineUsers();

	public void ChatroomMessageAdded(String msg, String name);
	

}
