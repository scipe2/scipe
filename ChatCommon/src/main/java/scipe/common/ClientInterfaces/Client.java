package scipe.common.ClientInterfaces;

import javax.jms.Message;
import javax.jms.MessageListener;

// Handles all of the logic of the application.
public abstract class Client implements FeedbackListener, MessageListener {
	
	// Gets the current client's IP address
	public abstract String getIP();
	
	/*
	 *  Sets the instance of the class that is in charge
	 *  of sending and receiving messages from the server;
	 *  
	 *  Make sure to set the MessageListener of the MessageHandler!
	 */
	public abstract void setMessageHandler(MessageHandler handler);
	
	// Sets the class in charge of displaying all of the information to
	// the user
	//
	// Make sure to set the FeedbackListener of the MessageDisplay!
	public abstract void setMessageDisplay(MessageDisplay display);
	
	// We need to implement this because of MessageListner
	public abstract void onMessage( Message msg );
	
	public abstract void run();

}
