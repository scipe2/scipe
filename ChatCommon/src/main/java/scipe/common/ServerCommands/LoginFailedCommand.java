package scipe.common.ServerCommands;

import javax.swing.JOptionPane;

import scipe.common.ClientInterfaces.MessageDisplay;
import scipe.common.Command.Command;

public class LoginFailedCommand extends Command {

	private static final long serialVersionUID = -5933188070261346252L;
	private String msg;

	public LoginFailedCommand(String msg){
		this.msg = msg;
	}
	
	@Override
	public void execute() throws Exception {
		MessageDisplay display = context.getBean(MessageDisplay.class);
		display.showLogin();
		JOptionPane.showMessageDialog(null,msg);
	}

}
