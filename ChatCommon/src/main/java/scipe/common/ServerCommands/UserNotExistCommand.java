package scipe.common.ServerCommands;

import javax.swing.JOptionPane;

import scipe.common.ClientInterfaces.MessageDisplay;
import scipe.common.Command.*;

public class UserNotExistCommand extends Command {

	private static final long serialVersionUID = 8018572818962967318L;
    private String msg;
    
    public UserNotExistCommand(String msg){
    	this.msg = msg;
    }
	
	@Override
	public void execute() throws Exception {
		MessageDisplay display = context.getBean(MessageDisplay.class);
		display.showLogin();
		JOptionPane.showMessageDialog(null,msg);
	}

}
