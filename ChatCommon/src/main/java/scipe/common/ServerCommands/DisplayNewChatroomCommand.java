package scipe.common.ServerCommands;

import scipe.common.ClientInterfaces.MessageDisplay;
import scipe.common.Command.Command;



public class DisplayNewChatroomCommand extends Command {

	private static final long serialVersionUID = 1L;
	private String nameOfChatroom;
	
	public DisplayNewChatroomCommand( String nameOfChatroom){
	this.nameOfChatroom = nameOfChatroom;
	}
	@Override
	public void execute() throws Exception {
		MessageDisplay display = context.getBean( MessageDisplay.class );
		display.specificMessage(this.nameOfChatroom);
		String msg = "Welcome to Chatroom: " + nameOfChatroom;
		display.showChatroomMessage(msg, nameOfChatroom);
	}

}
