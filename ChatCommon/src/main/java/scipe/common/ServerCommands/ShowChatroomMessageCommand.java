package scipe.common.ServerCommands;

import scipe.common.ClientInterfaces.MessageDisplay;
import scipe.common.Command.Command;

public class ShowChatroomMessageCommand extends Command {

	private static final long serialVersionUID = 8415745143703989318L;
	private String msg;
	private String chatroom;
	
	public ShowChatroomMessageCommand(String msg, String chatroom){
		this.msg = msg;
		this.chatroom = chatroom;
	}
	
	@Override
	public void execute() throws Exception {
		MessageDisplay display = context.getBean( MessageDisplay.class);
		display.showChatroomMessage(msg,chatroom);
	}

}
