package scipe.common.ServerCommands;

import java.util.ArrayList;

import scipe.common.ClientInterfaces.MessageDisplay;
import scipe.common.Command.Command;
import scipe.common.ServerInterfaces.User;

public class ListOnlineUsersCommand extends Command {
	
	private static final long serialVersionUID = -1651129203397114640L;
	private ArrayList<String> onlineUsers;
	private ArrayList<String> chatrooms;
	public ListOnlineUsersCommand( ArrayList<String> onlineUsers, ArrayList<String> chatrooms ){
		this.onlineUsers = onlineUsers;
		this.chatrooms = chatrooms;
	}
	
	@Override
	public void execute() throws Exception {
		if( this.context == null ){
			throw new Exception("Context is null");
		}
		MessageDisplay display = context.getBean( MessageDisplay.class);
		display.showUsers(onlineUsers);
		display.showChatrooms(chatrooms);
	}

}
