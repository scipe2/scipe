package scipe.common.ServerCommands;

import scipe.common.ClientInterfaces.MessageDisplay;
import scipe.common.Command.Command;

public class ShowBroadcastMessageCommand extends Command {

	private static final long serialVersionUID = -761655817691733778L;
	private String msg;
	
	public ShowBroadcastMessageCommand(String msg){
		this.msg = msg;
	}
	
	@Override
	public void execute() throws Exception {
		MessageDisplay display = context.getBean( MessageDisplay.class);
		display.showMessage(msg);
	}

}
