package scipe.common.ServerCommands;

import scipe.common.ClientCommands.GetOnlineUsersCommand;
import scipe.common.ClientCommands.UpdateOnlineUserListCommand;
import scipe.common.ClientInterfaces.MessageDisplay;
import scipe.common.ClientInterfaces.MessageHandler;
import scipe.common.Command.Command;

public class LoginSuccesCommand extends Command {

	private static final long serialVersionUID = 7060693121392918447L;
	private String msg;
	
	public LoginSuccesCommand(String msg){
		this.msg = msg;
	}
	
	@Override
	public void execute() throws Exception {
		MessageDisplay display = context.getBean( MessageDisplay.class );
		MessageHandler messenger = context.getBean( MessageHandler.class );
		display.showChat();
		display.showMessage( "Welcome to the chat, " + msg + "!" );
		display.showHome();
		messenger.sendCommand( new UpdateOnlineUserListCommand(messenger.getLocalDestination()));
	}

}
