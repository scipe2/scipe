package scipe.common.ServerInterfaces;

import javax.jms.MessageListener;

public abstract class Server implements MessageListener{
	
	// Sets the class that broadcasts all the messages to the clients
	// and handles all of the connection requests
	//
	// Make sure to set the ServerListener for this BroadcastHandler
	public abstract void setMessageHandler(ServerMessageHandler handler);
	
	// Access the message handler that is needed to communicate with the clients
	public abstract ServerMessageHandler getMessageHandler();
	
	// Sets the database that will store and retrieve all of the server's 
	// information
	public abstract void setDatabase(Database database);
	
	// Gets the IP Address of this server
	public abstract String getIP();
	
	// Get the port number of this server
	public abstract int getPort();
	
	public abstract void run();

}
