package scipe.common.ServerInterfaces;

import javax.jms.Destination;

public class ScipeUser implements User {
	
	private static final long serialVersionUID = 7371653571852673431L;
	private String username;
	private String password;
	private boolean loggedIn = true;
	private Destination client;
	
	public ScipeUser(String username, String password, Destination client){
		this.username = username;
		this.password = password;
		this.client = client;
	}

	public boolean login(String password, Destination client) {
		if (this.password.equals(password)){
			loggedIn = true;
			this.client = client;
			return true;
		} 
		return false;
	}

	public boolean logoff() {
		loggedIn = false;
		// Disconnect the client
		return true;
	}

	public String getUsername() {
		return username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public boolean changePassword(String oldPassword, String newPassword) {
		if (password.equals(oldPassword)){
			password = newPassword;
			return true;
		}
		return false;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public Destination getCurrentClient() {
		return client;
	}

	public void addClientDestination(Destination client) {
		this.client = client;
	}

	public void removeClientDestination(Destination client) {
		this.client = null;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			return ((User) obj).getUsername().equals(getUsername());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return getUsername().hashCode();
	}
	
}
