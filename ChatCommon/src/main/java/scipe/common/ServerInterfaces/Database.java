package scipe.common.ServerInterfaces;

import java.util.ArrayList;

import javax.jms.Destination;

public interface Database {
	
	// Gets the user with the following email
	// Returns null if none exists
	public User getUser(String username);
	
	// Gets the chatroom with the following title
	// Returns null if none exists
	public Chatroom getChatroom(String title);
	
	// Get an ArrayList with all of the Users
	public ArrayList<User> getAllUsers();
	
	// Get an ArrayList with all online Users
	public ArrayList<User> getAllOnlineUsers();
	
	// Get an ArrayList with all of the chatrooms
	public ArrayList<Chatroom> getAllChatrooms();
	
	// Adds the user to the database
	public boolean addUser(User user);
	
	// Adds the chatroom to the database
	public boolean addChatroom(Chatroom chatroom);
	
	// Checks to see if the user exists in the database
	public boolean hasUser(String user);
	
	// Checks to see if the chatroom exists in the database
	public boolean hasChatroom(Chatroom chatroom);
	
	// Removes the user from the database. 
	// Returns True if a user has been removed.
	public boolean removeUser(User user);
	
	// Removes the chatroom from the database. 
    // Returns True if a chatroom has been removed.
	public boolean removeChatroom(Chatroom chatroom);
	
	// Returns the number of users in the database
	public int getNumUsers();
	
	// Returns the number of chatrooms in the database
	public int getNumChatrooms();
	
	public void writeIntoDBFile();
	
	public User getUser(Destination client);

}
