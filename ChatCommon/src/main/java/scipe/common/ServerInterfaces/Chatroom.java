package scipe.common.ServerInterfaces;

import java.util.ArrayList;

public interface Chatroom {
	
	// Gets the name of the chatroom
	public String getTitle();
	
	// Returns a list of users in the chatroom
	public ArrayList<User> getUsers();
	
	// Adds the user to the chatroom
	public void addUser(User user);
	
	// Removes the user from the chatroom
	public void removeUser(User user);
	
	// Gets the number of users in this chatroom
	public int getNumUsers();

}
