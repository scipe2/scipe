package scipe.common.ServerInterfaces;

import java.util.ArrayList;
import java.util.HashMap;

import javax.jms.Destination;
import javax.jms.MessageListener;
import javax.jms.Session;

import scipe.common.Command.Command;
import scipe.common.ServerCommands.ShowBroadcastMessageCommand;

// This class should always be listening 
public interface ServerMessageHandler {
	
	// Sends a message back to a specific client. Returns true if
	// the client's message was successfully sent
	public boolean sendChatroomMessage(String msg, Chatroom chatroom);
	public boolean sendMessage(String msg, Destination client);
	public boolean sendMessages(HashMap<String, String> msgs, Destination client);
	
	// Send Command cmd to Destination
	public boolean sendCommand( Command cmd, Destination client );
	
	// Sends a message to all of the clients in the ArrayList.
	// Returns an ArrayList of all the ClientSockets who did NOT
	// receive the message.
	public ArrayList<Destination> broadcastMessageToClients(String msg, ArrayList<Destination> clients);
	
	public ArrayList<Destination> broadcastMessagesToClients(HashMap<String, String> msgs, ArrayList<Destination> clients);
	
	public ArrayList<User> broadcastMessage(String msg, ArrayList<User> users);
	
	public ArrayList<User> broadcastMessage(HashMap<String, String> msgs, ArrayList<User> users);
	
	// Sends message to everyone excluding User with usrname exc
	public ArrayList<User> broadcastMessageExc(String msg, ArrayList<User> users, String exc);
	
	// Sets the class that needs to deal with all of the messages sent
	// by the clients
	// Returns true if the messageListener was successfully set.
	public boolean setMessageListener(MessageListener listener);
	
	// This returns the session that is being managed by this ServerMessageHandler
	public Session getSession();

}
