package scipe.common.ServerInterfaces;

import java.util.ArrayList;

public class ScipeChatroom implements Chatroom {
	
	private String name;
	private ArrayList<User> users;
	
	public ScipeChatroom(String name){
		this.name = name;
		this.users = new ArrayList<User>();
	}

	public String getTitle() {
		return name;
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void addUser(User user) {
		users.add(user);
	}

	public void removeUser(User user) {
		users.remove(users);
	}

	public int getNumUsers() {
		return users.size();
	}

}
