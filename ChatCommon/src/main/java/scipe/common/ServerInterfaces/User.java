package scipe.common.ServerInterfaces;

import java.io.Serializable;

import javax.jms.Destination;

public interface User extends Serializable{
	
	// Returns true if the password is valid
	public boolean login(String password, Destination client);
	
	// Returns true if the user is currently logged
	// in and has been properly logged off
	public boolean logoff();
	
	// Returns the user's email
	public String getUsername();
	
	// Returns true if the old password is authenticated
	// and the new password is valid
	public boolean changePassword(String oldPassword, 
			String newPassword);
	
	// Returns true if the user is currently logged in
	public boolean isLoggedIn();
	
	// Returns a list of all the clients that this user is currently
	// logged into.
	public Destination getCurrentClient();
	
	// Associate this socket with this user
	public void addClientDestination(Destination client);
	
	// Disassociate this socket with this user
	public void removeClientDestination(Destination client);

	public String getPassword();
	
}
