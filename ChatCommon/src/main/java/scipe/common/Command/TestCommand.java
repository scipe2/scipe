package scipe.common.Command;

import scipe.common.ClientInterfaces.MessageDisplay;

public class TestCommand extends Command {
	
	private static final long serialVersionUID = 8772428659215982044L;
	private String msg;

	public TestCommand(String msg ) {
		this.msg = msg;
	}

	@Override
	public void execute() {	
		MessageDisplay display = context.getBean( MessageDisplay.class );
		display.showChat();
		display.showMessage(msg);
	}

}
