package scipe.common.Command;

import java.io.Serializable;

import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.springframework.context.ApplicationContext;

public abstract class Command implements Serializable {

	private static final long serialVersionUID = -9141196574292035127L;
	protected transient ApplicationContext context;
	protected static final String COMMAND_LABEL = "command";
	
	public abstract void execute() throws Exception;
	
	public void setContext( ApplicationContext context ){
		this.context = context;
	}

	public static Command extractCommand(Message message) throws Exception {
		if (!(message instanceof ObjectMessage)) {
			throw new Exception();
		}
		Object obj = ((ObjectMessage) message).getObject();
		if (!(obj instanceof Command)) {
			throw new Exception();
		}
		return (Command) obj;
	}
	
}