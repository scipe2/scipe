package scipe.common.ClientCommands;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Destination;

import scipe.common.Command.Command;
import scipe.common.ServerCommands.ListOnlineUsersCommand;
import scipe.common.ServerInterfaces.Chatroom;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ServerMessageHandler;
import scipe.common.ServerInterfaces.User;

public class UpdateOnlineUserListCommand extends Command {

	private static final long serialVersionUID = 1986521226512177468L;

	private Destination dest;

	public UpdateOnlineUserListCommand( Destination dest ){
		this.dest = dest;
	}

	@Override
	public void execute() throws Exception {
		ServerMessageHandler messenger = context.getBean(ServerMessageHandler.class);
		Database database = context.getBean( Database.class );
		ArrayList<User> userList = database.getAllOnlineUsers();
		ArrayList<Chatroom> chatroomList = database.getAllChatrooms();
		ArrayList<String> onlineUsers = new ArrayList<String>();
		ArrayList<String> chatrooms = new ArrayList<String>();
		
		for( User u : userList )
			onlineUsers.add(u.getUsername());
		for( Chatroom c : chatroomList)
			chatrooms.add(c.getTitle());
		
		for(User u : userList){
			Command command = new ListOnlineUsersCommand(onlineUsers,chatrooms);
			messenger.sendCommand(command, u.getCurrentClient());
		}
		
	}

}
