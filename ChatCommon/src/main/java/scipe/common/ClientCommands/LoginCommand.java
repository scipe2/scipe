package scipe.common.ClientCommands;

import javax.jms.Destination;

import scipe.common.Command.Command;
import scipe.common.ServerCommands.LoginFailedCommand;
import scipe.common.ServerCommands.LoginSuccesCommand;
import scipe.common.ServerCommands.UserNotExistCommand;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ServerMessageHandler;

public class LoginCommand extends Command {
	
	private static final long serialVersionUID = 1986521226512177468L;
	private String username;
	private String password;
	private Destination dest;
	
    public LoginCommand( String username, String password, Destination dest ){
    	this.username = username;
    	this.password = password;
    	this.dest = dest;
    }
    
    @Override
	public void execute() throws Exception {
		
    	if( this.context == null ){
			throw new Exception("Context is null");
		}
		Database database = context.getBean( Database.class );
		ServerMessageHandler messenger = context.getBean(ServerMessageHandler.class);
		Command command;
		
		if (!database.hasUser(username)) {
			command = new UserNotExistCommand(username + " does not exist.");
			messenger.sendCommand(command, dest);
			return;
		}
		if (database.getUser(username).login(password, dest) ) {
			command = new LoginSuccesCommand( username );
			messenger.sendCommand(command, dest);
			messenger.broadcastMessageExc(username + " has joined the chatroom!", database.getAllUsers(), username);
		} else {
			command = new LoginFailedCommand("Incorrect password. try again.");
			messenger.sendCommand(command, dest);
		}
		
		
	}
    
}
