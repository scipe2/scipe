package scipe.common.ClientCommands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.jms.Destination;

import scipe.common.Command.Command;
import scipe.common.ServerCommands.DisplayNewChatroomCommand;
import scipe.common.ServerCommands.LoginFailedCommand;
import scipe.common.ServerCommands.LoginSuccesCommand;
import scipe.common.ServerCommands.UserNotExistCommand;
import scipe.common.ServerInterfaces.Chatroom;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ScipeChatroom;
import scipe.common.ServerInterfaces.ServerMessageHandler;
import scipe.common.ServerInterfaces.User;

public class ChatRoomJoinedCommand extends Command {

	private static final long serialVersionUID = 1986521226512177468L;
	private ArrayList<String> friends;
	private Destination dest;
	
	
    public ChatRoomJoinedCommand(ArrayList<String> friends,  Destination dest){
    	this.friends = friends;
    	this.dest = dest;
    }
    
    @Override
	public void execute() throws Exception {
    	if( this.context == null ){
			throw new Exception("Context is null");
		}
		Database database = context.getBean( Database.class );
		ServerMessageHandler messenger = context.getBean(ServerMessageHandler.class);
		String nameOfCreator= database.getUser(dest).getUsername();
		String nameOfChatroom = "";
		
		friends.add( nameOfCreator );
		Set<String> set = new HashSet<String>(friends);
		friends = new ArrayList<String>( set );
		Collections.sort( friends );
		
		nameOfChatroom += friends.get(0);
		for ( int i = 1; i < friends.size(); ++i){
			nameOfChatroom += ", " + friends.get(i);
		}
		Chatroom chatroom = new ScipeChatroom(nameOfChatroom);
		for (String friend : friends){ 
			chatroom.addUser(database.getUser(friend));
		}
		database.addChatroom(chatroom);
		Command cmd = new DisplayNewChatroomCommand(nameOfChatroom);
		for (String friend : friends){
			messenger.sendCommand(cmd,database.getUser(friend).getCurrentClient());
		}
		
	}
    
}
