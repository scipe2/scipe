
package scipe.common.ClientCommands;

import javax.jms.Destination;
import javax.jms.TextMessage;

import scipe.common.Command.Command;
import scipe.common.ServerCommands.RegisterSuccessfulCommand;
import scipe.common.ServerCommands.UserExistCommand;
import scipe.common.ServerCommands.UserNotExistCommand;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ScipeUser;
import scipe.common.ServerInterfaces.ServerMessageHandler;
import scipe.common.ServerInterfaces.User;

public class RegisterCommand extends Command {

	private static final long serialVersionUID = -350257327635836507L;
	private String username;
	private String password;
	private Destination dest;

	public RegisterCommand(String username, String password, Destination dest) {
		this.dest = dest;
		this.username = username;
		this.password = password;
	}

	@Override
	public void execute() throws Exception {
		ServerMessageHandler messenger = context.getBean(ServerMessageHandler.class);
		Database database = context.getBean(Database.class);
		Command command;
		
		if (database.hasUser(username)) {
			System.out.println("User in the DB");
			command = new UserExistCommand(username + " does exist.");
			messenger.sendCommand(command, dest);
			return;
			
		}

		else {
			System.out.println("Register successful");
			command = new RegisterSuccessfulCommand(username + " registration successful!");
			messenger.sendCommand(command, dest);
			
			
			User user = new ScipeUser(username, password, dest);
			database.addUser(user);
			
			
			database.writeIntoDBFile();
			
			return;
			
		}
			

	}

}
