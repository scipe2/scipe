package scipe.common.ClientCommands;

import java.util.ArrayList;

import javax.jms.Destination;

import scipe.common.Command.Command;
import scipe.common.ServerCommands.ListOnlineUsersCommand;
import scipe.common.ServerInterfaces.Chatroom;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ServerMessageHandler;
import scipe.common.ServerInterfaces.User;

public class GetOnlineUsersCommand extends Command {

	private static final long serialVersionUID = 2076974558137697654L;
	private Destination dest;
	
	public GetOnlineUsersCommand( Destination dest ){
		this.dest = dest;
	}
	
	@Override
	public void execute() throws Exception {
		if( this.context == null ){
			throw new Exception("Context is null");
		}
		
		Database database = context.getBean( Database.class );
		ServerMessageHandler messenger = context.getBean(ServerMessageHandler.class);
		ArrayList<Chatroom> chatrooms = database.getAllChatrooms();
		ArrayList<String> nameOfChatrooms = new ArrayList<String>();
		ArrayList<String> users = new ArrayList<String>();
		for ( Chatroom c : chatrooms){
			nameOfChatrooms.add(c.getTitle());
		}
		ArrayList<User> onlineUsers = database.getAllOnlineUsers();
		for ( User u: onlineUsers){
			users.add(u.getUsername());
		}
		Command command = new ListOnlineUsersCommand( users, nameOfChatrooms);
		messenger.sendCommand(command, dest);
	}

}
