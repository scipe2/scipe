package scipe.common.ClientCommands;

import javax.jms.Destination;
import javax.jms.TextMessage;

import scipe.common.Command.Command;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ServerMessageHandler;

public class SendBrodcaseMessageCommand extends Command {

	private static final long serialVersionUID = -350257327635836507L;
	private String msg;
	private Destination dest;

	public SendBrodcaseMessageCommand(String msg, Destination dest) {
		this.msg = msg;
		this.dest = dest;
	}

	@Override
	public void execute() throws Exception {
		ServerMessageHandler messenger = context.getBean(ServerMessageHandler.class);
		Database database = context.getBean(Database.class);
		messenger.broadcastMessage(database.getUser(dest).getUsername() + ": " + msg, database.getAllUsers());
	}

}
