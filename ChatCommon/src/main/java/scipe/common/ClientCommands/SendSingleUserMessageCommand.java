package scipe.common.ClientCommands;

import java.util.ArrayList;

import javax.jms.Destination;
import javax.jms.TextMessage;

import scipe.common.Command.Command;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ServerMessageHandler;
import scipe.common.ServerInterfaces.User;

public class SendSingleUserMessageCommand extends Command {

	private static final long serialVersionUID = -6955004676558505024L;
	private String username;
	private String msg;
	private Destination dest;
	
	public SendSingleUserMessageCommand( String msg, String username, Destination dest ){
		this.username = username;
		this.msg = msg;
		this.dest = dest;
	}

	@Override
	public void execute() throws Exception {
		ServerMessageHandler messenger = context.getBean(ServerMessageHandler.class);
		Database database = context.getBean(Database.class);
		TextMessage txtMsg = messenger.getSession().createTextMessage(msg);
		ArrayList<User> temp = new ArrayList<User>();
		temp.add(database.getUser(username));
		messenger.broadcastMessage(database.getUser(dest).getUsername() + ": "
				+ txtMsg.getText(), temp );	
	}

}
