package scipe.common.ClientCommands;

import javax.jms.Destination;

import scipe.common.Command.Command;
import scipe.common.ServerInterfaces.Chatroom;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ServerMessageHandler;

public class SendChatroomMessageCommand extends Command {
	
	private static final long serialVersionUID = -9138368279509044658L;
	private String nameOfChatroom;
	private String msg;
	private Destination dest;
	
	public SendChatroomMessageCommand( String msg, String nameOfChatroom, Destination dest ){
		this.nameOfChatroom = nameOfChatroom;
		this.msg = msg;
		this.dest = dest;
	}

	@Override
	public void execute() throws Exception {
		ServerMessageHandler messenger = context.getBean(ServerMessageHandler.class);
		Database database = context.getBean(Database.class);
		Chatroom chatroom = database.getChatroom(nameOfChatroom);
		messenger.sendChatroomMessage(database.getUser(dest).getUsername() + ": " + msg, chatroom);
	}

}
