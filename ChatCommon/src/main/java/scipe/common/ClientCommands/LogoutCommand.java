package scipe.common.ClientCommands;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Destination;

import scipe.common.ClientInterfaces.MessageHandler;
import scipe.common.Command.Command;
import scipe.common.ServerCommands.ListOnlineUsersCommand;
import scipe.common.ServerInterfaces.Chatroom;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ServerMessageHandler;
import scipe.common.ServerInterfaces.User;

public class LogoutCommand extends Command {
	
	private static final long serialVersionUID = 2784734951656076455L;
	private Destination dest;
	
    public LogoutCommand( Destination dest ){
    	this.dest = dest;
    }
    
    @Override
	public void execute() throws Exception {
    	if( this.context == null ){
			throw new Exception("Context is null");
		}
		Database database = context.getBean( Database.class );
		ServerMessageHandler messanger = context.getBean( ServerMessageHandler.class );
		
		User u = database.getUser(dest);
		u.logoff();
		messanger.broadcastMessage( u.getUsername() + " has logged off.", database.getAllOnlineUsers() );
		ArrayList<User> userList = database.getAllOnlineUsers();
		ArrayList<Chatroom> chatroomList = database.getAllChatrooms();
		ArrayList<String> users = new ArrayList<String>();
		ArrayList<String> chatrooms = new ArrayList<String>();
		for( User user : userList)
			users.add(user.getUsername());
		for( Chatroom c : chatroomList)
			chatrooms.add(c.getTitle());
		
		for(User user : userList){
			messanger.sendCommand( new ListOnlineUsersCommand(users, chatrooms), user.getCurrentClient());
		}
		
	}
    
}
