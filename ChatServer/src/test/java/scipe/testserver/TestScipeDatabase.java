package scipe.testserver;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import scipe.common.ServerInterfaces.Chatroom;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ScipeUser;
import scipe.common.ServerInterfaces.User;
import scipe.server.ScipeDatabase;

public class TestScipeDatabase {

	@Test
	public void testScipeDatabase() {
		Database db = new ScipeDatabase();	
	}

	@Test
	public void testGetUser() {
		User one = new ScipeUser("foo", "123", null);
		Database db = new ScipeDatabase();
		db.addUser(one);
		assertNotNull(db.getUser("foo"));
		assertNull(db.getUser("akdjfk"));

	}

	@Test
	public void testGetChatroom() {

	}

	@Test
	public void testGetAllUsers() {
		User one = new ScipeUser("foo", "123", null);
		User two = new ScipeUser("bar", "123", null);
		Database db = new ScipeDatabase();
		db.addUser(one);
		db.addUser(two);
		ArrayList<User> users = db.getAllUsers();
		for (User u : users) {
			if (u.getUsername().equals("foo")
					|| u.getUsername().equals("bar"))
				return;
			
		}


	}

	@Test
	public void testGetAllChatrooms() {
		//fail("Not yet implemented");
	}

	@Test
	public void testAddUser() {
		User one = new ScipeUser("foo", "123", null);
		User two = new ScipeUser("bar", "123", null);
		Database db = new ScipeDatabase();
		db.addUser(one);
		db.addUser(two);
		assertNotNull(db.getUser("foo"));
		assertNotNull(db.getUser("bar"));
		assertNull(db.getUser("dfasdf"));
	}

	@Test
	public void testAddChatroom() {
		//fail("Not yet implemented");
	}

	@Test
	public void testHasUser() {
		User one = new ScipeUser("foo", "123", null);
		User two = new ScipeUser("bar", "123", null);
		Database db = new ScipeDatabase();
		db.addUser(one);
	;
		assertTrue(db.hasUser( one.getUsername() ));
		assertFalse(db.hasUser( two.getUsername() ));
	}

	@Test
	public void testHasChatroom() {
		//fail("Not yet implemented");
	}

	@Test
	public void testRemoveUser() {
		//fail("Not yet implemented");
	}

	@Test
	public void testRemoveChatroom() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetNumUsers() {
		User one = new ScipeUser("foo", "123", null);
	    Database db = new ScipeDatabase();
	    db.addUser(one);
	    
	    assertEquals(db.getNumUsers(), 4);
	}

	@Test
	public void testGetNumChatrooms() {
		//fail("Not yet implemented");
	}
	
	@Test
	public void testLogin_Success() {
	    User one = new ScipeUser("foo", "123", null);
	    Database db = new ScipeDatabase();
	    db.addUser(one);

	    String inputUsername = "foo";
	    String inputPassword = "123";
	    if(db.getUser(inputUsername)==null){
	        fail();
	    } else {
	        Boolean isLogedIn = db.getUser(inputUsername).login(inputPassword, null);
	        assertTrue(isLogedIn);
	    }
	}

	@Test
	public void testLogin_Fail() {
	    User one = new ScipeUser("foo", "123", null);
	    Database db = new ScipeDatabase();
	    db.addUser(one);

	    String inputUsername = "foo";
	    String inputPassword = "12345";
	    if(db.getUser(inputUsername)==null){
	        fail();
	    } else {
	        Boolean isLogedIn = db.getUser(inputUsername).login(inputPassword, null);
	        assertFalse(isLogedIn);
	    }
	}

}