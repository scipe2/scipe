package scipe.testserver;

import static org.junit.Assert.*;

import org.junit.Test;

import scipe.common.ServerInterfaces.ScipeUser;
import scipe.common.ServerInterfaces.User;

public class TestScipeUser {

	@Test
	public void testHashCode() {

		User one = new ScipeUser("foo", "123", null);
		User two = new ScipeUser("foo", "123", null);
		User three = new ScipeUser("bar", "123", null);

		assertEquals(one.hashCode(), two.hashCode());
		assertNotEquals(one.hashCode(), three.hashCode());

	}

	@Test
	public void testScipeUser() {
		User one = new ScipeUser("foo", "123", null);
		User two = new ScipeUser("foo", "123", null);
	}

	@Test
	public void testLogin() {
		User one = new ScipeUser("foo", "123", null);
		assertTrue(one.login("123", null ) ); // need to fix this
		assertFalse(one.login("12", null ) );
	}

	@Test
	public void testLogoff() {
		User one = new ScipeUser("foo", "123", null);
		one.logoff();
		assertFalse(one.isLoggedIn());
	}

	@Test
	public void testGetUsername() {
		User one = new ScipeUser("foo", "123", null);
		assertEquals("foo", one.getUsername());
	}

	@Test
	public void testChangePassword() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsLoggedIn() {
		User one = new ScipeUser("foo", "123", null);
		assertTrue(one.login("123", null )); //Might need to fix
		assertTrue(one.isLoggedIn());
	}

	@Test
	public void testGetCurrentClient() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddClientDestination() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemoveClientDestination() {
		fail("Not yet implemented");
	}

	@Test
	public void testEqualsObject() {
		User one = new ScipeUser("foo", "123", null);
		User two = new ScipeUser("foo", "123", null);
		User three = new ScipeUser("bar", "123", null);

		assertEquals(one, two);
		assertNotEquals(one, three);
	}

}
