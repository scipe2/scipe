package scipe.server;


//package scipe.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jms.Destination;

import scipe.common.ServerInterfaces.Chatroom;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ScipeUser;
import scipe.common.ServerInterfaces.User;

public class ScipeDatabase implements Database {

	private HashMap<String, User> usersMap;
	private HashMap<String, Chatroom> chatMap; 
	public static final String DEF_DATABASE_LOCATION = "DBFile";
	
	public ScipeDatabase(){
		this(DEF_DATABASE_LOCATION);
	}
	
	public ScipeDatabase(String fileName){
		chatMap = new HashMap<String, Chatroom>();
		usersMap = new HashMap<String, User>();
		
		File file = new File(fileName); //Reading data from local DB file
		
		BufferedReader br = null;
		String name = "";
		String password = "";
		
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		String line = null;
		String []rec = null;
		
		try {
			while ((line=br.readLine())!=null) {
				rec = line.split("\t");		//Local DB file format is : User+'\t'+password
				name = rec[0].trim();
				password = rec[1].trim();
				System.out.println(name + '\t' + password); // Debug Info
				
				ScipeUser user = new ScipeUser(name,password,null);
				user.logoff();		//Default logoff
				usersMap.put(user.getUsername(), user);
			}
		} catch (IOException e) {
		
			e.printStackTrace();
		}
		
	}
	
	public void writeIntoDBFile(){
		//File file = new File(DEF_DATABASE_LOCATION); //Reading data from local DB file
		
		BufferedWriter br = null;
		
			try {
				br = new BufferedWriter(new FileWriter(DEF_DATABASE_LOCATION));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			try {
				List<User> userList = this.getAllUsers();
				for(User u : userList){
					br.write(u.getUsername()+"\t"+u.getPassword()+"\n");
				}
				br.close();


			} catch (IOException e) {		
				e.printStackTrace();
			}
		
		
	}
	public User getUser(String username) {
		if (usersMap.containsKey(username)){
			return usersMap.get(username);
		}
		return null;
	}
	
	public User getUser(Destination client) {
		// Implement better performance using binary search later or hash map
		for (User u : usersMap.values()) {
			if( u.getCurrentClient() == null )
				continue;
			if (u.getCurrentClient().equals(client)) {
				return u;
			}
		}
		return new ScipeUser("YOU HAVE A BUG", "", null);
	}

	public Chatroom getChatroom(String title) {
		if (chatMap.containsKey(title)){
			return chatMap.get(title);
		}
		return null;
	}

	public ArrayList<User> getAllUsers() {
		return new ArrayList<User>(usersMap.values());
	}
	
	public ArrayList<User> getAllOnlineUsers(){
		ArrayList<User> allUsers = getAllUsers();
		ArrayList<User> onLine = new ArrayList<User>();
		for( User u : allUsers )
		 if( u.isLoggedIn() )
			onLine.add( u );
		return onLine;
	}

	public ArrayList<Chatroom> getAllChatrooms() {
		return new ArrayList<Chatroom>(chatMap.values());
	}

	public boolean addUser(User user) {
		if (!hasUser(user.getUsername())) {
			usersMap.put(user.getUsername(), user);
			return true;
		}
		return false;
	}

	public boolean addChatroom(Chatroom chatroom) {
		if (! hasChatroom(chatroom)) {
			chatMap.put(chatroom.getTitle(), chatroom);
			return true;
		}
		return false;		
	}

	public boolean hasUser(String user) {
		return usersMap.containsKey(user);
	}

	public boolean hasChatroom(Chatroom chatroom) {
		return chatMap.containsKey(chatroom.getTitle()); 
	}

	public boolean removeUser(User user) {
		if(usersMap.containsKey(user)){
			usersMap.remove(user);
			return true; 
		}
		return false;
	}

	public boolean removeChatroom(Chatroom chatroom) {
		if(chatMap.containsKey(chatroom)){
			chatMap.remove(chatroom);
			return true; 
		}
		return false;
	}

	public int getNumUsers() {
		return usersMap.size();
	}

	public int getNumChatrooms() {
		return chatMap.size();
	}

	public boolean addUser(String user) {
		// TODO Auto-generated method stub
		return false;
	}

}



/**
import java.util.ArrayList;
import java.util.HashMap;

import javax.jms.Destination;

public class ScipeDatabase implements Database {

	private HashMap<String, User> usersMap;
	private HashMap<String, Chatroom> chatMap; 
	
	public ScipeDatabase(){
		usersMap = new HashMap<String, User>();
		chatMap = new HashMap<String, Chatroom>();
	}
	
	public User getUser(String username) {
		if (usersMap.containsKey(username)){
			return usersMap.get(username);
		}
		return null;
	}
	
	public User getUser(Destination client) {
		// Implement better performance using binary search later or hash map
		for (User u : usersMap.values()) {
			if (u.getCurrentClient().equals(client)) {
				return u;
			}
		}
		return new ScipeUser("YOU HAVE A BUG", "", null);
	}

	public Chatroom getChatroom(String title) {
		if (chatMap.containsKey(title)){
			return chatMap.get(title);
		}
		return null;
	}

	public ArrayList<User> getAllUsers() {
		return new ArrayList<User>(usersMap.values());
	}

	public ArrayList<Chatroom> getAllChatrooms() {
		return new ArrayList<Chatroom>(chatMap.values());
	}

	public boolean addUser(User user) {
		if (! hasUser(user)) {
			usersMap.put(user.getUsername(), user);
			return true;
		}
		return false;
		
	}

	public boolean addChatroom(Chatroom chatroom) {
		if (! hasChatroom(chatroom)) {
			chatMap.put(chatroom.getTitle(), chatroom);
			return true;
		}
		return false;		
	}

	public boolean hasUser(User user) {
		return usersMap.containsKey(user.getUsername());
	}

	public boolean hasChatroom(Chatroom chatroom) {
		return chatMap.containsKey(chatroom.getTitle()); 
	}

	public boolean removeUser(User user) {
		if(usersMap.containsKey(user)){
			usersMap.remove(user);
			return true; 
		}
		return false;
	}

	public boolean removeChatroom(Chatroom chatroom) {
		if(chatMap.containsKey(chatroom)){
			chatMap.remove(chatroom);
			return true; 
		}
		return false;
	}

	public int getNumUsers() {
		return usersMap.size();
	}

	public int getNumChatrooms() {
		return chatMap.size();
	}

}
*/
