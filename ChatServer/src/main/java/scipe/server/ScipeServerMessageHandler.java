package scipe.server;

import java.util.ArrayList;
import java.util.HashMap;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;

import scipe.common.Command.*;
import scipe.common.ServerCommands.ShowBroadcastMessageCommand;
import scipe.common.ServerCommands.ShowChatroomMessageCommand;
import scipe.common.ServerInterfaces.Chatroom;
import scipe.common.ServerInterfaces.Server;
import scipe.common.ServerInterfaces.ServerMessageHandler;
import scipe.common.ServerInterfaces.User;

public class ScipeServerMessageHandler implements ServerMessageHandler {

	private static final String QUEUENAME = "clientMessages";
	private MessageProducer producer;
	private Session session;
	private Destination incomingMessages;
	private HashMap<Destination, String> userList;
	private MessageConsumer consumer;
	
	public ScipeServerMessageHandler(Server s) {
		
		try {
			BrokerService broker = new BrokerService();
			broker.addConnector("tcp://" + s.getIP() + ":" + s.getPort());
			broker.setPersistent(false);
			broker.start();
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
					"tcp://" + s.getIP() + ":" + s.getPort());
			Connection connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			incomingMessages = session.createQueue(QUEUENAME);
			producer = session.createProducer(null);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			consumer = session.createConsumer(incomingMessages);
			setMessageListener( s );			
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public boolean sendChatroomMessage(String msg, Chatroom chatroom){
		Command cmd = new ShowChatroomMessageCommand(msg, chatroom.getTitle());
		for( User u : chatroom.getUsers() )
			sendCommand(cmd, u.getCurrentClient());
		return true;
	}
	
	public boolean sendMessage(String msg, Destination client) {
		Command cmd = new ShowBroadcastMessageCommand(msg);
		return sendCommand(cmd, client); 
	}
	
	public boolean sendCommand(Command cmd, Destination client){
		// Handles null cases
		if (client == null) {
			return false;
		}
		try{
			ObjectMessage message = session.createObjectMessage();
			message.setObject(cmd);
			producer.send(client, message);
			return true;
		}
		catch( Exception e ){
			return false;
		}
	}
	
	public Session getSession(){
		return session;
	}
	
	public boolean sendMessages(HashMap<String, String> msgs, Destination client) {
		// Handles null cases
		if (client == null) {
			return false;
		}
		try {
			TextMessage message = session.createTextMessage();
			for (String title : msgs.keySet()){
				message.setStringProperty(title, msgs.get(title));
			}
			producer.send(client, message);
			return true;
		} catch (JMSException e) {
			return false;
		}
	}

	public ArrayList<Destination> broadcastMessageToClients(String msg ,ArrayList<Destination> clients) {
		ArrayList<Destination> failedClients = new ArrayList<Destination>();
		for (Destination client : clients) {
			if (!sendMessage(msg, client)){
				failedClients.add(client);
			}
		}
		return failedClients;
	}
	
	public ArrayList<User> broadcastMessage(String msg, ArrayList<User> users) {
		ArrayList<User> failedUsers = new ArrayList<User>();
		for (User user : users) {
			if (!sendMessage(msg, user.getCurrentClient() )){
				failedUsers.add(user);
			}
		}
		return failedUsers;
		
		/*ArrayList<User> failedUsers = new ArrayList<User>();
		for (User user : users) {
			if (!sendMessage(user.getCurrentClient(), msg)){
				failedUsers.add(user);
			}
		}
		return failedUsers;*/	
	}
	
	public ArrayList<User> broadcastMessageExc(String msg, ArrayList<User> users, String exc) {
		ArrayList<User> failedUsers = new ArrayList<User>();
		for (User user : users) {
			if( user.getUsername().equals(exc))
				continue;
			if (!sendMessage(msg, user.getCurrentClient())){
				failedUsers.add(user);
			}
		}
		return failedUsers;
	}

	public boolean setMessageListener(MessageListener listener) {
		try {
			consumer.setMessageListener(listener);
			return true;
		} catch (JMSException e) {
			e.printStackTrace();
			
			return false;
		}
	}

	/* MIGHT BE NEEDED LATER ON */
	public ArrayList<Destination> broadcastMessagesToClients(HashMap<String, String> msgs, ArrayList<Destination> clients) {
		ArrayList<Destination> failedClients = new ArrayList<Destination>();
		for (Destination client : clients) {
			if (! sendMessages(msgs, client)){
				failedClients.add(client);
			}
		}
		return failedClients;
	}

	public ArrayList<User> broadcastMessage(HashMap<String, String> msgs, ArrayList<User> users) {
		ArrayList<User> failedUsers = new ArrayList<User>();
		for (User user : users) {
			if (! sendMessages(msgs,user.getCurrentClient())){
				failedUsers.add(user);
			}
		}
		return failedUsers;
	}

}
