package scipe.server;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import scipe.common.ClientInterfaces.MessageHandler;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.Server;

public class ScipeServerApplication {
	
	public static void main(String[] args){
		AnnotationConfigApplicationContext context = 
		          new AnnotationConfigApplicationContext(ScipeServer.class);
		Server server = context.getBean(Server.class);
		server.run();
	}

}
