package scipe.server;

import java.util.HashMap;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import scipe.common.Command.Command;
import scipe.common.ServerInterfaces.Database;
import scipe.common.ServerInterfaces.ScipeUser;
import scipe.common.ServerInterfaces.Server;
import scipe.common.ServerInterfaces.ServerMessageHandler;

@Configuration
public class ScipeServer extends Server {

	private static final String IP_ADDR = "localhost";
	private static final int PORT = 61616;
	private ServerMessageHandler messenger;
	private Database database;
	private @Autowired ApplicationContext context;
	
	@Bean
	Database dataBase(){
		return new ScipeDatabase();
	}
	
	@Bean
	ServerMessageHandler messenger( Server scipeServer ){
		return new ScipeServerMessageHandler( scipeServer );
	}
	
	public ScipeServer(){
	}
	
	@Override
	public String getIP() {
		return IP_ADDR;
	}

	@Override
	public int getPort() {
		return PORT;
	}

	@Override
	public void setMessageHandler(ServerMessageHandler handler) {
		this.messenger = handler;
	}

	@Override
	public void setDatabase(Database database) {
		this.database = database;
	}

	@Override
	public ServerMessageHandler getMessageHandler() {
		return messenger;
	}
	
	public Database getDatabase() {
		return database;
	}
	
	@Override
	public void run() {
		ServerMessageHandler messanger = context.getBean(ServerMessageHandler.class);
		Database database = context.getBean(Database.class);
		setDatabase(database);
		setMessageHandler(messanger);
	}

	public void onMessage(Message message) {

		try {
			Command cmd = Command.extractCommand(message);
			System.err.println( "SERVER RECIEVED COMMAND: " + cmd + "FROM CLEINT" );
			cmd.setContext(context);
			cmd.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/* THIS REALLY ISNT NECESSARY DUE TO THE COMMANDS*/
		
		/*
		 * try { String cmd = message.getStringProperty("command"); if
		 * (cmd.equals("PLAIN")) { handlePlainMessage(message); } else if
		 * (cmd.equals("LOGIN")) { handleLoginMessage(message); } else if
		 * (cmd.equals("SIGNUP")) { handleSignupMessage(message); } } catch
		 * (JMSException e1) { // Message not formatted correctly return; }
		 */
	}

	
	/*THIS REALLY ISNT NECESSARY DUE TO THE COMMANDS*/
	private void handleLoginMessage(String username, String password, Destination dest) throws JMSException {
		
		/*String user = username;
		String pass = password;

		if (database.hasUser(user)) {
			if (database.getUser(user).login(pass, dest) ) {
				HashMap<String, String> info = new HashMap<String, String>();
				info.put("command", "LOGIN");
				info.put("message", user + " has joined the chatroom!");
				messenger.sendMessages(dest, info);
			} else {
				HashMap<String, String> info = new HashMap<String, String>();
				info.put("command", "LOGIN FAILED");
				info.put("error", "Password is incorrect");
				messenger.sendMessages(dest, info);
			}
		} else {
			HashMap<String, String> info = new HashMap<String, String>();
			info.put("command", "LOGIN FAILED");
			info.put("error", user + " does not exist.");
			messenger.sendMessages(dest, info);
		}*/
	}

	/*THIS REALLY ISNT NECESSARY DUE TO THE COMMANDS*/
	private void handlePlainMessage(Message message) throws JMSException {
		/*TextMessage txtMsg = (TextMessage) message;
		messenger.broadcastMessage(database.getAllUsers(),
				database.getUser(message.getJMSReplyTo()).getUsername() + ": "
						+ txtMsg.getText());*/
	}

	private void handleSignupMessage(Message message) throws JMSException {
		String user = message.getStringProperty("username");
		String pass = message.getStringProperty("password");

		if (database.hasUser(user)) {
			HashMap<String, String> info = new HashMap<String, String>();
			info.put("command", "SIGNUP TAKEN");
			info.put("error", user + " is a taken username!");
			messenger.sendMessages(info, message.getJMSReplyTo());
		} else {
			database.addUser(new ScipeUser(user, pass, message.getJMSReplyTo()));
			HashMap<String, String> info = new HashMap<String, String>();
			info.put("command", "SIGNUP");
			info.put("message", user + " has joined Scipe!");
			messenger.sendMessages(info, message.getJMSReplyTo());
		}
	}

}
