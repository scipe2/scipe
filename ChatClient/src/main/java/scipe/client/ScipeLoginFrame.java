package scipe.client;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.SpringLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JPasswordField;

import scipe.common.ClientInterfaces.FeedbackListener;

public class ScipeLoginFrame extends ScipeFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private static FeedbackListener currentListener;
	private JTextField username;
	private JPasswordField password;
	private JTextField textIP;

	public ScipeLoginFrame(String name) {
		super(name);
		setResizable(false);
		getContentPane().setBackground(Color.WHITE);
		getContentPane().setForeground(Color.WHITE);
		setSize(300, 500);
		setLocation(810, 290);
		setBackground(Color.WHITE);
		getContentPane().setLayout(null);

		// JLabel label = new JLabel();
		// label.setIcon(new ImageIcon);
		username = new JTextField();
		username.setBounds(141, 311, 114, 19);
		getContentPane().add(username);
		username.setColumns(10);

		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setBounds(29, 314, 77, 15);
		getContentPane().add(lblUserName);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(36, 359, 70, 15);
		getContentPane().add(lblPassword);

		JButton btnLogin = new JButton("login");
		btnLogin.setBounds(29, 431, 77, 27);
		btnLogin.setFont(btnLogin.getFont().deriveFont(btnLogin.getFont().getStyle() | Font.BOLD));
		btnLogin.setForeground(Color.BLACK);
		btnLogin.setBackground(Color.WHITE);
		getContentPane().add(btnLogin);
		btnLogin.setOpaque(false);

		JButton btnRegister = new JButton("register");
		btnRegister.setBounds(164, 432, 91, 25);
		btnRegister.setBackground(Color.WHITE);
		getContentPane().add(btnRegister);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(36, 88, 0, 0);
		lblNewLabel
				.setIcon(new ImageIcon(
						"C:\\eclipseworkspace\\scipe\\ChatClient\\src\\main\\java\\scipe\\client\\SCIPE_LOGO.jpg"));
		getContentPane().add(lblNewLabel);

		password = new JPasswordField();
		password.setBounds(141, 356, 114, 19);
		getContentPane().add(password);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(38, 70, 219, 130);
		lblNewLabel_1.setIcon(new ImageIcon(ScipeLoginFrame.class.getResource("/scipe/client/1472396_10153504775665475_2125155284_n.jpg")));
		getContentPane().add(lblNewLabel_1);
		
		textIP = new JTextField();
		textIP.setBounds(141, 401, 114, 19);
		getContentPane().add(textIP);
		textIP.setColumns(10);
		
		JLabel lblIpAddress = new JLabel("Server IP");
		lblIpAddress.setBounds(36, 404, 70, 15);
		getContentPane().add(lblIpAddress);

		btnLogin.addActionListener(this);
		btnRegister.addActionListener(this);
	}

	@Override
	public void setListener(FeedbackListener listener) {
		currentListener = listener;
	}

	public void actionPerformed(ActionEvent e) {
		if (currentListener == null) {
			return;
		}
		if (e.getActionCommand().equals("login")) {
			if (username.getText().equals("") || password.getText().equals("") || textIP.getText().equals("")) {
				JOptionPane
						.showMessageDialog(null, "All fields must be filled");
			} else {
				currentListener.loginPerformed(username.getText(),
						password.getText(), textIP.getText());
			}
		}
		if (e.getActionCommand().equals("register")) {
			currentListener.registerClicked();
		}
	}
}