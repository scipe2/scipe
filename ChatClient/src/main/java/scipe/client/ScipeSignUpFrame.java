package scipe.client;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import scipe.common.ClientInterfaces.FeedbackListener;

import java.awt.Color;

public class ScipeSignUpFrame extends ScipeFrame implements ActionListener {

	private static final long serialVersionUID = -166815554424234946L;
	private JPanel contentPane;
	private JTextField username;
	private FeedbackListener currentListener;
	private JPasswordField password;
	private JPasswordField retype;
	private JTextField textServer;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public ScipeSignUpFrame(String a) {
		super(a);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBounds(810, 290, 300, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel, "name_1103116860724");

		username = new JTextField();
		username.setBounds(71, 117, 116, 19);
		username.setColumns(10);

		JLabel lblNewLabel = new JLabel("Username:");
		lblNewLabel.setBounds(71, 90, 77, 15);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(71, 159, 75, 15);
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel lblRetypePassword = new JLabel("Retype Password:");
		lblRetypePassword.setBounds(71, 245, 129, 15);

		JButton btnSignMeUp = new JButton("Sign me Up!");
		btnSignMeUp.setBounds(71, 388, 118, 25);
		btnSignMeUp.setBackground(Color.WHITE);

		password = new JPasswordField();
		password.setBounds(73, 272, 116, 19);

		retype = new JPasswordField();
		retype.setBounds(71, 186, 116, 19);
		panel.setLayout(null);
		panel.add(btnSignMeUp);
		panel.add(lblRetypePassword);
		panel.add(password);
		panel.add(retype);
		panel.add(username);
		panel.add(lblNewLabel);
		panel.add(lblPassword);
		
		textServer = new JTextField();
		textServer.setBounds(73, 337, 114, 19);
		panel.add(textServer);
		textServer.setColumns(10);
		
		JLabel lblServerIp = new JLabel("Server IP:");
		lblServerIp.setBounds(71, 310, 70, 15);
		panel.add(lblServerIp);

		btnSignMeUp.addActionListener(this);

	}

	@SuppressWarnings("deprecation")
	public void actionPerformed(ActionEvent e) {
		// only for the sign me up button
		if (retype.getText().equals("") || password.getText().equals("")
				|| password.getText().equals("") || textServer.getText().equals("")) {
			JOptionPane.showMessageDialog(null,
					"Please Fill All of the text fields");
		} else if (!retype.getText().equals(password.getText())) {
			JOptionPane.showMessageDialog(null,
					"The two inputed passwords do not match!");
			retype.setText("");
			password.setText("");
		} else {
			
			currentListener.registerPerformed(username.getText(), password.getText(), textServer.getText());
		}
	}

	@Override
	public void setListener(FeedbackListener listener) {
		currentListener = listener;

	}
}