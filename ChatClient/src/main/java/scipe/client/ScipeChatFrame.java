package scipe.client;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import scipe.common.ClientInterfaces.FeedbackListener;

public class ScipeChatFrame extends ScipeFrame implements ActionListener {
	
	private static final long serialVersionUID = 5997089946307766788L;
	
	private TextField tf;
	private TextArea ta;
	private FeedbackListener currentListener;

	ScipeChatFrame(String name) {
		
		super(name);

		tf = new TextField(40);
		ta = new TextArea();
		tf.setSize(40, 40);
		tf.setFont(new Font("Arial",Font.BOLD,15));
		ta.setFont(new Font("Arial",Font.BOLD,15));
	
		
		this.add(tf,BorderLayout.SOUTH);
		this.add(ta,BorderLayout.NORTH);
		this.setBounds(300,300,900,900);
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				currentListener.logoutPerformed();
				System.exit(0);
				//logout command
			}
		});
		
		tf.addActionListener(this);
		this.pack();
		this.setVisible(false);
		ta.setEditable(false);
		this.setResizable(false);
		
	}
	
	public void addMessage(String msg) {
		ta.append(msg +"\n");
	}
	
	public void clearMessages() {
		ta.setText("");
	}

	public void actionPerformed(ActionEvent e) {
		if (currentListener == null) {
			return;
		}
		String msg = tf.getText();
		tf.setText("");
		currentListener.messageAdded(msg);
	}

	@Override
	public void setListener(FeedbackListener listener) {
		currentListener = listener;
	}
	
}
