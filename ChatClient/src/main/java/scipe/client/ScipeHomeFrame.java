package scipe.client;

import javax.swing.JTabbedPane;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import scipe.common.ClientInterfaces.FeedbackListener;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;

public class ScipeHomeFrame extends ScipeFrame implements ListSelectionListener {

	private FeedbackListener currentListener;
	private DefaultListModel listModel;
	private static final long serialVersionUID = 1L;
	private JList listofUsers;
	private JButton btnMessage;
	private String name;
	private JList listChatrooms;
	private DefaultListModel listModelChatroom;

	public ScipeHomeFrame(String name) {
		super(name);
		getContentPane().setLayout(new CardLayout(0, 0));
		setBounds(810, 290, 300, 500);
		
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, "name_13055565938282");
		
		JPanel userTab = new JPanel();
		userTab.setBackground(Color.CYAN);
		tabbedPane.addTab("List Users", null, userTab, null);
		
		//create the list of users.
		listModel = new DefaultListModel();
		userTab.setLayout(null);
		listofUsers = new JList(listModel);
		listofUsers.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		listofUsers.setBounds(0, 5, 291, 360);
		userTab.add(listofUsers);
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				currentListener.logoutPerformed();
				System.exit(0);
				//logout command
			}
		});
		
		btnMessage = new JButton("Message");
		btnMessage.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> friends = new ArrayList<String>();
				//String user = (String)listofUsers.getSelectedValue();
			    Object[] users = listofUsers.getSelectedValues();
				System.out.println(users);
				for (Object friend : users){
					friends.add((String)friend);
				}
				if (friends == null){
					System.err.println("it is null");
					System.exit(ERROR);
				}
				currentListener.messageFriend(friends);
				//System.out.println(user);
				
			}
		});	
		btnMessage.setBounds(18, 393, 254, 25);
		userTab.add(btnMessage);
		//add a listener to see which one is selected
		listofUsers.addListSelectionListener(this);
		btnMessage.setEnabled(false);
		
		JPanel chatroomTab = new JPanel();
		chatroomTab.setBackground(Color.GREEN);
		tabbedPane.addTab("List Chatrooms", null, chatroomTab, null);
		chatroomTab.setLayout(null);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener( new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				//listModel.clear();
				currentListener.refreshOnlineUsers();
				
			}
		});
		btnRefresh.setBounds(28, 387, 217, 25);
		chatroomTab.add(btnRefresh);
		
		listModelChatroom = new DefaultListModel();
		listChatrooms = new JList( listModelChatroom );
		listChatrooms.setEnabled(false);
		listChatrooms.setValueIsAdjusting(true);
		listChatrooms.setBounds(0, 5, 291, 346);
		chatroomTab.add(listChatrooms);
		
	}
	
	public void clearJlist(){
		listModelChatroom.clear();
		listModel.clear();
		
	}
	public void setListener(FeedbackListener listener) {
		currentListener = listener;

	}
	
	public void addChatroom(String nameChatroom){
		System.out.println("fajksadfklasjfklsajfdklsajfklsajf;lajfd;ksaljfd;ljd;lfj");
		listModelChatroom.addElement(nameChatroom);
	}
	public void addUser(String user){
			listModel.addElement(user);
	}
//continue here, find out how to get list of users

	public void valueChanged(ListSelectionEvent e) {
		btnMessage.setEnabled(true);
		//JList source = (JList) e.getSource();
		//String friend = (String)source.getSelectedValue();
		//System.out.println(friend);
	    //currentListener.messageFriend(friend);
		}
}