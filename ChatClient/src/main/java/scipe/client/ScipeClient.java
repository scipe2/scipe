package scipe.client;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.jms.Message;
import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import scipe.common.ClientCommands.ChatRoomJoinedCommand;
import scipe.common.ClientCommands.GetOnlineUsersCommand;
import scipe.common.ClientCommands.LoginCommand;
import scipe.common.ClientCommands.LogoutCommand;
import scipe.common.ClientCommands.RegisterCommand;
import scipe.common.ClientInterfaces.Client;
import scipe.common.ClientInterfaces.MessageDisplay;
import scipe.common.ClientInterfaces.MessageHandler;
import scipe.common.Command.*;

@Configuration
public class ScipeClient extends Client {

	private MessageHandler messenger;
	private MessageDisplay display;
	private @Autowired ApplicationContext context;
	private String IP;

	@Bean
	Client scipeClient() {
		return new ScipeClient();
	}

	@Bean
	MessageDisplay display(final ScipeClient scipeClient) {
		return new ScipeGUIDisplay() {{
			setFeedbackListener(scipeClient);
		}};
	}

	@Bean
	MessageHandler messanger(Client scipeClient) {
		return new ScipeMessageHandler(scipeClient);
	}

	public ScipeClient() {
	}
	
	@Override
	public String getIP() {
		return IP;
		/*
		try {
			return InetAddress.getLocalHost().toString();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return null;
		}
		*/
	}
	
	@Override
	public void setMessageHandler(MessageHandler handler) {
		messenger = handler;
	}

	@Override
	public void setMessageDisplay(MessageDisplay display) {
		this.display = display;
	}

	public void run() {
		
		MessageDisplay messageDisplay = context.getBean(MessageDisplay.class);
		
		
		setMessageDisplay(messageDisplay);
		display.showLogin();
	}
    
	public void onMessage(Message message) {
		try {
			Command cmd = Command.extractCommand(message);
			cmd.setContext(context);
			cmd.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/* THIS REALLY ISNT NECESSARY DUE TO THE COMMANDS*/
		
		/*
		 * try { String cmd = message.getStringProperty("command"); if
		 * (cmd.equals("LOGIN")) { loginSuccess(message); } else if
		 * (cmd.equals("LOGIN FAILED")) { loginFailed(message); } else if
		 * (cmd.equals("SHOW")){ messageAdded(message); } else if
		 * (cmd.equals("SIGNUP TAKEN")) { showError(message); } else if
		 * (cmd.equals("SIGNUP")) { display.showChat(); messageAdded(message); }
		 * else if (cmd.equals("PLAIN")){ try {
		 * display.showMessage(((TextMessage) message).getText()); } catch
		 * (JMSException e) {
		 * 
		 * } } } catch (JMSException e1) { // Message not formatted correctly
		 * return; }
		 */
	}
	
	private void initMessageHandler() {
		messenger = context.getBean(MessageHandler.class);
		messenger.setServer(getIP());
		messenger.reconnect();
	}

	public void loginPerformed(String user, String password, String IP) {
		this.IP = IP;
		initMessageHandler();
		
		Command cmd = new LoginCommand(user, password,
				messenger.getLocalDestination());
		if (!messenger.sendCommand(cmd)) {
			if (messenger.reconnect()) {
				if (!messenger.sendCommand(cmd)) {
					JOptionPane.showMessageDialog(null,
							"Could not connect to the server");
				}
			} 
			else {
				JOptionPane.showMessageDialog(null,
						"Could not connect to the server");
			}
		}
	}
	
	public void registerPerformed(String username, String password, String IP) {
		this.IP = IP;
		initMessageHandler();
		Command cmd = new RegisterCommand(username, password, 
				messenger.getLocalDestination());
		if (!messenger.sendCommand(cmd)) {
			if (messenger.reconnect()) {
				if (!messenger.sendCommand(cmd)) {
					JOptionPane.showMessageDialog(null,
							"Registering....");
				}
			} 
			else {
				JOptionPane.showMessageDialog(null,
						"Registering....");
			}
		}
		
	}

	public void logoutPerformed() {
		Command command = new LogoutCommand( messenger.getLocalDestination() );
		messenger.sendCommand( command );
	}

	public void messageFriend(ArrayList<String> friends) {
		Command cmd = new ChatRoomJoinedCommand( friends, messenger.getLocalDestination() );
		messenger.sendCommand(cmd);
	}

	public void ChatroomMessageAdded(String msg, String name) {
		messenger.sendChatroomMessage(msg,name);
	}
	
	public void messageAdded(String msg) {
		messenger.sendMessage(msg);
	}
	
	public void refreshOnlineUsers(){
		Command cmd = new GetOnlineUsersCommand(messenger.getLocalDestination());
		messenger.sendCommand(cmd);
	}
	public void chatroomJoined(String chatroom) {
		// TODO Auto-generated method stub
	}
	
	public void signedUp(String email, String password) {
		HashMap<String, String> info = new HashMap<String, String>();
		info.put("command", "SIGNUP");
		info.put("username", email);
		info.put("password", password);
		if (!messenger.sendMessages(info)) {
			if (messenger.reconnect()) {
				if (!messenger.sendMessages(info)) {
					JOptionPane.showMessageDialog(null,
							"Could not connect to the server");
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"Could not connect to the server");
			}
		}
	}

	public void registerClicked() {
		display.showSignUp();
	}

	public void messageFriend(String friend) {
		display.specificMessage(friend);
	}

	// Temporary Commands
	public void loginSuccess(Message message) {
		/*display.showChat();
		try {
			display.showMessage(message.getStringProperty("message"));
		} catch (JMSException e) {

		}*/
	}

	public void loginFailed(Message message) {
		/*try {
			JOptionPane.showMessageDialog(null,
					message.getStringProperty("error"));
		} catch (JMSException e) {

		}*/
	}

	public void messageAdded(Message message) {
		/*try {
			display.showMessage(message.getStringProperty("message"));
		} catch (JMSException e) {

		}*/
	}

	public void showError(Message message) {
		/*try {
			JOptionPane.showMessageDialog(null,
					message.getStringProperty("error"));
		} catch (HeadlessException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}*/
	}






}
