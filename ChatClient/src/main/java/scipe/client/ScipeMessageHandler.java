package scipe.client;

import java.net.URISyntaxException;
import java.util.HashMap;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.management.ObjectName;

import org.apache.activemq.ActiveMQConnection;

import scipe.common.ClientCommands.SendChatroomMessageCommand;
import scipe.common.ClientCommands.SendBrodcaseMessageCommand;
import scipe.common.ClientCommands.SendSingleUserMessageCommand;
import scipe.common.ClientInterfaces.MessageHandler;
import scipe.common.Command.*;

public class ScipeMessageHandler implements MessageHandler {

	private static final String QUEUENAME = "clientMessages";
	private static final int DEF_PORT = 61616;
	private static final String DEF_IP = "localhost";
	private String serverIP = DEF_IP;
	private int serverPort = DEF_PORT;
	private Session session;
	private ActiveMQConnection connection;
	private Destination serverDestination;
	private MessageProducer producer;
	private Destination thisClientDestination;
	private MessageConsumer receivedMessages;
	private Thread closer;
	private MessageListener currentMessageListener;
	private boolean connectable;

	private boolean initActiveMQ() {
		try {
			connection = ActiveMQConnection.makeConnection(
					/* Constants.USERNAME, Constants.PASSWORD, */
					"tcp://" + serverIP + ":" + serverPort);
			connection.start();
			closer = CloseHook.registerCloseHook(connection);
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			serverDestination = session.createQueue(QUEUENAME);
			producer = session.createProducer(getServer());
			thisClientDestination = session.createTemporaryQueue();
			receivedMessages = session.createConsumer(getLocalDestination());
			return true;
		} catch (JMSException e) {
			e.printStackTrace();
			return false;
		} catch (URISyntaxException e) { // Error thrown by ActiveMQConnection constructor
			e.printStackTrace();
			return false;
		}
	}
	
	public ScipeMessageHandler() {
		reconnect();
	}

	public ScipeMessageHandler(MessageListener listener) {
		currentMessageListener = listener;
		
	}
	
	public boolean sendCommand( Command cmd ){
		if (!isConnectable()){
			return false;
		}
		try{
			System.err.println("SENDING COMMAND: " + cmd + " FROM CLIENT");
			ObjectMessage message = session.createObjectMessage();
			message.setObject(cmd);
			message.setJMSReplyTo(getLocalDestination());
			producer.send(message);
			return true;
		}
		catch( Exception e ){
			return false;
		}
	}

	public boolean sendMessage(String msg) {
		Command cmd = new SendBrodcaseMessageCommand(msg,getLocalDestination());
		sendCommand(cmd);
		return false;
	}
	
	public void sendChatroomMessage(String msg,String chatroom){
		Command cmd = new SendChatroomMessageCommand(msg, chatroom, getLocalDestination());
		sendCommand(cmd);
	}
	
	public boolean sendMessages(HashMap<String, String> msgs) {
		/*
		try{
			System.err.println("SENDING MESSAGE");
			ObjectMessage message = session.createObjectMessage();
			message.setObject(new TestCommand("HElllllllooooo"));
			message.setStringProperty("command", "PLAIN");
			message.setJMSReplyTo(thisClientDestination);
			producer.send(message);
			return true;
		}
		catch( Exception e ){
			return false;
		}
		
		// Handles null cases
		if ( ! isConnectable() ) {
			return false;
		}
		
		try {
			TextMessage message = session.createTextMessage();
			for (String title : msgs.keySet()){
				message.setStringProperty(title, msgs.get(title));
			}
			message.setJMSReplyTo(thisClientDestination);
			messenger.send(message);
			return true;
		} catch (JMSException e) {
			return false;
		}*/
		return false;
	}
	
	public void setServer() {
		setServer(DEF_IP, DEF_PORT);
	}
	
	public void setServer(String ip) {
		setServer(ip, DEF_PORT);
		/*
		setServer(ip, DEF_PORT);
		*/
	}

	public void setServer(String ip, int port) {
		serverIP = ip;
		serverPort = port;
		/*
		closer.start();
		serverIP = ip;
		serverPort = port;
		initActiveMQ();
		setMessageListener(currentMessageListener);
		*/
	}

	public Destination getServer() {
		return serverDestination;
	}
	
	public Destination getLocalDestination(){
		return thisClientDestination;
	}

	public boolean isConnectable() {
		// TODO Auto-generated method stub
		return connectable;
	}
	
	public boolean reconnect() {
		connectable = initActiveMQ();
		setMessageListener(currentMessageListener);
		return connectable;
	}

	public boolean setMessageListener(MessageListener listener) {
		// Handles when receivedMessages is NULL for improper initiation
		if (! isConnectable() ) { 
			return false;
		}
		try {
			receivedMessages.setMessageListener(listener);
			currentMessageListener = listener;
			return true;
		} catch (JMSException e) {
			// e.printStackTrace();
			return false;
		}
	}

	/*
	 * This inner class is used to make sure we clean up when the client closes
	 */
	static private class CloseHook extends Thread {
		ActiveMQConnection connection;

		private CloseHook(ActiveMQConnection connection) {
			this.connection = connection;
		}

		public static Thread registerCloseHook(ActiveMQConnection connection) {
			Thread ret = new CloseHook(connection);
			Runtime.getRuntime().addShutdownHook(ret);
			return ret;
		}

		public void run() {
			try {
				System.out.println("Closing ActiveMQ connection");
				connection.close();
			} catch (JMSException e) {
				/*
				 * This means that the connection was already closed or got some
				 * error while closing. Given that we are closing the client we
				 * can safely ignore this.
				 */
			}
		}
	}
	
}
