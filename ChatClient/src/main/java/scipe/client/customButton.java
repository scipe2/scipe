package scipe.client;

import javax.swing.JButton;

public class customButton extends JButton{
	
	private static final long serialVersionUID = -5663568335857435288L;

	public customButton(String text)
	{
		super(text);
		//setBorder(null);
		
		setBorderPainted(true);
		//setContentAreaFilled(false);
		setOpaque(false);
	}
}