package scipe.client;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import scipe.common.ClientInterfaces.FeedbackListener;

public class ScipeSpecificMessageFrame extends ScipeFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5997089946307766788L;
	
	private TextField tf;
	private TextArea ta;
	private FeedbackListener currentListener;
	private String name;

	ScipeSpecificMessageFrame(String name) {
		super(name);
		this.name = name;

		tf = new TextField(40);
		ta = new TextArea();
		tf.setSize(40, 40);
		tf.setFont(new Font("Arial",Font.BOLD,15));
		ta.setFont(new Font("Arial",Font.BOLD,15));
	
		
		this.add(tf,BorderLayout.SOUTH);
		this.add(ta,BorderLayout.NORTH);
		this.setBounds(1150,300,900,900);
		/*
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		*/
		
		tf.addActionListener(this);
		this.pack();
		this.setVisible(false);
		ta.setEditable(false);
		this.setResizable(false);
		//tf.addKeyListener(this);
		
	}
	
	public void addMessage(String msg) {
		ta.append(msg +"\n");
	}
	
	public void clearMessages() {
		ta.setText("");
	}

	public void actionPerformed(ActionEvent e) {
		if (currentListener == null) {
			return;
		}
		System.out.println("hello");
		String msg = tf.getText();
		tf.setText("");
		currentListener.ChatroomMessageAdded(msg, name);
	}
	
	@Override
	public void setListener(FeedbackListener listener) {
		currentListener = listener;
	}

	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	/*

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER){
			String msg = tf.getText();
			tf.setText("");
			System.out.println(name);
			currentListener.specificMessageAdded(msg, name);
		}
		
	}
*/
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}