package scipe.client;

import java.util.ArrayList;
import java.util.HashMap;

import scipe.common.ClientInterfaces.FeedbackListener;
import scipe.common.ClientInterfaces.MessageDisplay;

public class ScipeGUIDisplay implements MessageDisplay {
	
	private HashMap<String, ScipeFrame> frames = new HashMap<String, ScipeFrame>();
	private HashMap<String, ScipeSpecificMessageFrame> specificmessages = new HashMap<String, ScipeSpecificMessageFrame>();
	private FeedbackListener currentListener;

	public ScipeGUIDisplay(){
		this("");
	}
	
	public ScipeGUIDisplay(String name){
		frames.put("login", new ScipeLoginFrame("login" + name));
		frames.get("login").setVisible(false);
		
		frames.put("signup", new ScipeSignUpFrame("sign" + name));
		frames.get("signup").setVisible(false);
		
		frames.put("global chatroom", new ScipeChatFrame("global chatroom" + name));
		frames.get("global chatroom").setVisible(false);
		
		frames.put("logout", new ScipeLoginFrame("logout" + name));
		frames.get("logout").setVisible(false);
		
		frames.put("home", new ScipeHomeFrame("home" + name));
		frames.get("home").setVisible(false);	
	}
	
	public void showUsers(ArrayList<String> friends) {
		frames.get("home").clearJlist();
		for (String friend: friends){
			frames.get("home").addUser(friend);
		}
	}

	public void showChatrooms(ArrayList<String> chatrooms) {
		for (String chatroom : chatrooms){
			frames.get("home").addChatroom(chatroom);
		}
	}

	public void showMessage(String msg) {
		((ScipeChatFrame) frames.get("global chatroom")).addMessage(msg);
	}
	
	public void showChatroomMessage(String msg, String chatroom){
		 specificmessages.get(chatroom).addMessage(msg);
	}

	public void exit() {
		// TODO Auto-generated method stub
		
	}

	public void showLogout() {
		// TODO Auto-generated method stub
	}

	public void setFeedbackListener(FeedbackListener listener) {
		currentListener = listener;
		for ( ScipeFrame frame : frames.values() ){
			frame.setListener(listener);
		}
	}

	public void addMessage(String username, String msg) {
		// TODO Auto-generated method stub
	}

	public void addMessage(String username, String msg, String color) {
		// TODO Auto-generated method stub
		
	}
	
	private void showFrame(String frameName) {
		for ( ScipeFrame frame : frames.values() ){
			frame.setVisible(false);
		}
		frames.get(frameName).setVisible(true);
	}

	private void showFrameAlso(String frameName){
		frames.get(frameName).setVisible(true);
	}
		
	public void showChat() {
		showFrame("global chatroom");
	}
	
	public void showLogin() {
		showFrame("login");
	}
	
	public void showSignUp(){
		showFrame("signup");
	}
	
	public void showHome(){
		showFrame("home");
		showFrameAlso("global chatroom");
		
	}
	
	public void specificMessage(String friend){
		if (specificmessages.containsKey(friend)){
			specificmessages.get(friend).setVisible(true);
		}else{
			specificmessages.put(friend, new ScipeSpecificMessageFrame(friend));
			specificmessages.get(friend).setVisible(true);
			specificmessages.get(friend).setListener(currentListener);						
		}
	}

}
