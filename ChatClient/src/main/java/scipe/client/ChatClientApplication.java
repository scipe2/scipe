package scipe.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import scipe.common.ClientInterfaces.Client;

public class ChatClientApplication {

	public static void main(String[] args) throws InterruptedException {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				ScipeClient.class);
		Client client = context.getBean(Client.class);
		client.run();
	}

}